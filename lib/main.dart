import 'package:flutter/material.dart';
import 'package:practice_app/pages/home_page.dart';
import 'package:practice_app/pages/loading.dart';
import 'package:practice_app/pages/select_location.dart';

void main() {
  runApp(MaterialApp(
    initialRoute: '/',
    routes: {
      '/': (context) => Loading(),
      '/home': (context) => home(),
      '/loc': (context) => SelectLocation(),
    },
  ));
}
