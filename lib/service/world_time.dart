import 'dart:convert';
import 'package:http/http.dart';
import 'package:intl/intl.dart';

class WorldTime{
  String location;
  late String time;
  String flag;
  String url;
  late bool isItDay;

  WorldTime({required this.location, required this.url, required this.flag});

  Future<void> pullTime() async{
    try{

      Response response = await get(Uri.parse('http://worldtimeapi.org/api/timezone/$url'));
      Map timeNow = jsonDecode(response.body);
      String dateTime = timeNow['datetime'];

      DateTime orgDate = DateTime.parse(dateTime.substring(0,26));

      isItDay = orgDate.hour > 6 && orgDate.hour < 17 ? true:false;
      time = DateFormat.jm().format(orgDate);

    }catch(e){
      print('The Error is: $e');
      time = 'Error Loading Time';
    }
  }


}