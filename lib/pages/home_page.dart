import 'package:flutter/material.dart';
import 'package:practice_app/pages/select_location.dart';

class home extends StatefulWidget {
  const home({Key? key}) : super(key: key);

  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {

  Map info = {};

  @override
  Widget build(BuildContext context) {

    info = info.isNotEmpty ? info : ModalRoute.of(context)!.settings.arguments as Map;

    String condition = info['isItDay'] ? 'day.jpg' : 'night.png';
    Color colors1 = info['isItDay'] ? Colors.blueGrey : Colors.indigo;
    String flag1 = info['flag'].toString();

    return Scaffold(
      backgroundColor: colors1,
      body: SafeArea(
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/$condition'),
                fit: BoxFit.cover,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 150, 0, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RaisedButton.icon(
                      onPressed: () async {
                        dynamic outcome =  await Navigator.pushNamed(context, '/loc');
                        setState(() {
                          info = {
                            'time': outcome['time'],
                            'location': outcome['location'],
                            'isItDay': outcome['isItDay'],
                            'flag': outcome['flag'],
                          };
                        });
                        },
                    color: colors1,
                      icon: Icon(
                        Icons.edit_location,
                        color: Colors.white,
                      ),
                      label: Text(
                        'Select Location',
                        style: TextStyle(
                          color: Colors.white
                        ),
                      ),
                  ),
                  SizedBox(height: 30),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        backgroundImage: AssetImage('assets/$flag1'),
                        radius: 28,
                      ),
                      SizedBox(width: 20),
                      Text(
                        info['location'],
                        style: TextStyle(
                          fontSize: 50,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Text(
                    info['time'],
                    style: TextStyle(
                      fontSize: 50,
                      color: Colors.green,
                    ),
                  ),
                ],
              ),
            ),
          ),
      ),
    );
  }
}
