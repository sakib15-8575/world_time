import 'package:flutter/material.dart';
import 'package:practice_app/service/world_time.dart';

class SelectLocation extends StatefulWidget {
  const SelectLocation({Key? key}) : super(key: key);

  @override
  _SelectLocationState createState() => _SelectLocationState();
}

class _SelectLocationState extends State<SelectLocation> {

  List<WorldTime> diffLocation = [
    WorldTime(location: 'Dhaka', url: 'Asia/Dhaka', flag: 'bd.jpg'),
    WorldTime(location: 'India', url: 'Asia/Kolkata', flag: 'india.png'),
    WorldTime(location: 'Nairobi', url: 'Africa/Nairobi', flag: 'nairobi.jpg'),
    WorldTime(location: 'New York', url: 'America/New_York', flag: 'ny.jpg'),
    WorldTime(location: 'Havana', url: 'America/Havana', flag: 'havana.jpg'),
    WorldTime(location: 'Sydney', url: 'Australia/Sydney', flag: 'sydney.jpg'),
    WorldTime(location: 'London', url: 'Europe/London', flag: 'london.jpg'),
    WorldTime(location: 'Berlin', url: 'Europe/Berlin', flag: 'berlin.png'),
    WorldTime(location: 'Helsinki', url: 'Europe/Helsinki', flag: 'helsinki.png'),
    WorldTime(location: 'Honolulu', url: 'Pacific/Honolulu', flag: 'honolulu.jpg'),
  ];

  void passTime(index) async{
    WorldTime obj2 = diffLocation[index];
    await obj2.pullTime();

    Navigator.pop(context, {
      'location': obj2.location,
      'flag': obj2.flag,
      'time': obj2.time,
      'isItDay': obj2.isItDay,
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Select Location'),
      ),
      backgroundColor: Colors.blueAccent,
      body: ListView.builder(
        itemCount: diffLocation.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 4),
            child: Card(
              child: ListTile(
                onTap: () {
                  passTime(index);
                },
                title: Text(diffLocation[index].location),
                leading: CircleAvatar(
                  backgroundImage: AssetImage(
                    'assets/${diffLocation[index].flag}'
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
